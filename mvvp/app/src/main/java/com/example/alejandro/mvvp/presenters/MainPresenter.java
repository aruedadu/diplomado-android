package com.example.alejandro.mvvp.presenters;

import com.example.alejandro.mvvp.helper.Constants;
import com.example.alejandro.mvvp.models.League;
import com.example.alejandro.mvvp.models.Teams;
import com.example.alejandro.mvvp.repositories.Repository;
import com.example.alejandro.mvvp.views.interfaces.IMainInterface;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class MainPresenter extends BasePresenter<IMainInterface> {

    public void createThreadGetLeagues() throws ExecutionException, InterruptedException {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    League leagues = repository.getLeagues();
                    getView().fillLeagues(leagues);
                    createThreadGetTeams(leagues.getLeagues().get(0).getIdLeague());
                } catch (final IOException e) {
                    getView().showAlertDialogUiThread(Constants.ERROR_TITTLE, Constants.DEFAULT_ERROR);
                }
            }
        });
        thread.start();
    }

    public void createThreadGetTeams(final String idLeague){
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Teams teams = repository.getTeams(idLeague);
                    getView().fillLeague(teams);
                    getView().goToListLeagues();
                } catch (final IOException e) {
                    getView().showAlertDialogUiThread(Constants.ERROR_TITTLE, Constants.DEFAULT_ERROR);
                }
            }
        });
        thread.start();
    }

}
