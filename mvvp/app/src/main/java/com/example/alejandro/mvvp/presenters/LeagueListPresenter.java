package com.example.alejandro.mvvp.presenters;

import com.example.alejandro.mvvp.models.EventsContainer;
import com.example.alejandro.mvvp.models.Teams;
import com.example.alejandro.mvvp.views.interfaces.ILeagueListInterface;

import java.io.IOException;

public class LeagueListPresenter extends BasePresenter<ILeagueListInterface> {

    public void createThreadGetTeams(final String idLeague){
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Teams teams = repository.getTeams(idLeague);
                    getView().fillLeague(teams);
                } catch (final IOException e) {
                    //getView().showAlertDialog(Constants.ERROR_TITTLE, Constants.DEFAULT_ERROR);
                }
            }
        });
        thread.start();
    }

    public void createThreadGetEvents(final String idTeam){
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    EventsContainer events = repository.getNextFiveEvents(idTeam);
                    getView().setEvents(events);
                    getView().intentToDetailTeam();
                } catch (final IOException e) {
                    //getView().showAlertDialog(Constants.ERROR_TITTLE, Constants.DEFAULT_ERROR);
                }
            }
        });
        thread.start();
    }
}
