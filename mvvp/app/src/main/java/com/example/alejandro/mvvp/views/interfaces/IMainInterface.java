package com.example.alejandro.mvvp.views.interfaces;

import com.example.alejandro.mvvp.models.League;
import com.example.alejandro.mvvp.models.Teams;
import com.example.alejandro.mvvp.views.IBaseView;

public interface IMainInterface extends IBaseView {
    public void fillLeagues(League leagues);
    public void fillLeague(Teams teams);
    public void goToListLeagues();
}
