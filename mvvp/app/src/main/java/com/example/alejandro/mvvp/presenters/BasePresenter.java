package com.example.alejandro.mvvp.presenters;

import com.example.alejandro.mvvp.helper.ValidateInternet;
import com.example.alejandro.mvvp.repositories.Repository;
import com.example.alejandro.mvvp.views.IBaseView;

public class BasePresenter<T extends IBaseView> {

    private ValidateInternet validateInternet;
    protected Repository repository;

    private T view;

    public void inject(T view, ValidateInternet validateInternet, Repository repository) {
        this.validateInternet = validateInternet;
        this.view = view;
        this.repository = repository;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public T getView() {
        return this.view;
    }
}
