package com.example.alejandro.mvvp.helper;

public class Constants {

    public final static String DEFAULT_ERROR = "Ha ocurrido un error al tratar de realizar la consulta";
    public final static String ERROR_TITTLE = "Error";
    public final static String FOUNDED_YEAR = "Fundado en: ";

}
