package com.example.alejandro.mvvp.views.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.alejandro.mvvp.R;
import com.example.alejandro.mvvp.helper.Constants;
import com.example.alejandro.mvvp.models.EventsContainer;
import com.example.alejandro.mvvp.models.Team;
import com.example.alejandro.mvvp.presenters.DetailTeamPresenter;
import com.example.alejandro.mvvp.views.BaseActivity;
import com.example.alejandro.mvvp.views.interfaces.IDetailTeamActivity;
import com.squareup.picasso.Picasso;

public class DetailTeamActivity extends BaseActivity<DetailTeamPresenter> implements IDetailTeamActivity {

    private ImageView ivTeamBanner;
    private ImageView ivTeamJersey;
    private ImageView ivInstaButton;
    private ImageView ivFacebookButton;
    private ImageView ivTwitterButton;
    private ImageView ivWebButton;
    private TextView tvTeamName;
    private TextView tvTeamDescription;
    private TextView tvTeamFoundation;
    private ListView eventsList;
    private EventsContainer eventsContainer;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_team);
        team = (Team) getIntent().getSerializableExtra("team");
        eventsContainer = (EventsContainer) getIntent().getSerializableExtra("events");
        mapComponents();
        initializeComponents();
        loadSocualMedia();
    }

    private void mapComponents() {
        ivTeamBanner = findViewById(R.id.iv_detailTeam_banner);
        ivTeamJersey = findViewById(R.id.iv_detail_jersey);
        tvTeamName = findViewById(R.id.tv_team_name_detail);
        eventsList = findViewById(R.id.lv_events);
        tvTeamDescription = findViewById(R.id.tv_team_description);
        tvTeamFoundation = findViewById(R.id.tv_team_foundation);
        ivInstaButton = findViewById(R.id.iv_insta_button);
        ivFacebookButton = findViewById(R.id.iv_facebook_button);
        ivTwitterButton = findViewById(R.id.iv_twitter_button);
        ivWebButton = findViewById(R.id.iv_web_button);
    }

    private void initializeComponents() {
        Picasso.get().load(team.getTeamBanner()).into(ivTeamBanner);
        Picasso.get().load(team.getTeamJersey()).into(ivTeamJersey);
        tvTeamName.setText(team.getTeamName());
        eventsList.setAdapter(
                new ArrayAdapter<>(DetailTeamActivity.this,
                        android.R.layout.simple_spinner_dropdown_item, eventsContainer.getEvents())
        );
        tvTeamDescription.setText(team.getTeamDescription());
        tvTeamFoundation.setText(Constants.FOUNDED_YEAR + team.getFormedYear());
        ivInstaButton.setVisibility(View.INVISIBLE);
        ivFacebookButton.setVisibility(View.INVISIBLE);
        ivTwitterButton.setVisibility(View.INVISIBLE);
        ivWebButton.setVisibility(View.INVISIBLE);
    }

    private void loadSocualMedia() {
        if (null != team.getInstagram() && !"".equals(team.getInstagram().trim())) {
            ivInstaButton.setVisibility(View.VISIBLE);
        }
        if (null != team.getInstagram() && !"".equals(team.getFacebook().trim())) {
            ivFacebookButton.setVisibility(View.VISIBLE);
        }
        if (null != team.getInstagram() && !"".equals(team.getTwitter().trim())) {
            ivTwitterButton.setVisibility(View.VISIBLE);
        }
        if (null != team.getInstagram() && !"".equals(team.getWebSite().trim())) {
            ivWebButton.setVisibility(View.VISIBLE);
        }
    }

    public void goToSite(View view) {
        String url = "http://";
        switch (view.getId()) {
            case R.id.iv_insta_button:
                url += team.getInstagram();
                break;
            case R.id.iv_facebook_button:
                url += team.getFacebook();
                break;
            case R.id.iv_twitter_button:
                url += team.getTwitter();
                break;
            case R.id.iv_web_button:
                url += team.getWebSite();
                break;
            default:
                break;
        }
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
