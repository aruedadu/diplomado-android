package com.example.alejandro.mvvp.services;

import com.example.alejandro.mvvp.models.EventsContainer;
import com.example.alejandro.mvvp.models.League;
import com.example.alejandro.mvvp.models.Teams;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IServices {

    @GET("all_leagues.php")
    Call<League> getLeagues();

    @GET("lookup_all_teams.php")
    Call<Teams> getTeams(@Query("id") String id);

    @GET("eventsnext.php")
    Call<EventsContainer> getNextFiveEvents(@Query("id") String id);

}