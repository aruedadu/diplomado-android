package com.example.alejandro.mvvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Leagues implements Serializable {

    /*
    idLeague	"4328"
    strLeague	"English Premier League"
    strSport	"Soccer"
    strLeagueAlternate	"Premier League"
     */

    @SerializedName("idLeague")
    @Expose
    private String idLeague;

    @SerializedName("strLeague")
    @Expose
    private String leagueName;

    @SerializedName("strSport")
    @Expose
    private String sportName;

    @SerializedName("strLeagueAlternate")
    @Expose
    private String alterLeagueName;

    public String getIdLeague() {
        return idLeague;
    }

    public void setIdLeague(String idLeague) {
        this.idLeague = idLeague;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public String getSportName() {
        return sportName;
    }

    public void setSportName(String sportName) {
        this.sportName = sportName;
    }

    public String getAlterLeagueName() {
        return alterLeagueName;
    }

    public void setAlterLeagueName(String alterLeagueName) {
        this.alterLeagueName = alterLeagueName;
    }

    @Override
    public String toString() {
        return leagueName;
    }

    /*@Override
    public String toString() {
        return this.leagueName;
    }*/
}
