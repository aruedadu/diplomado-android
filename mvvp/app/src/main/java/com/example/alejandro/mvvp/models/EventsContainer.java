package com.example.alejandro.mvvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class EventsContainer implements Serializable {

    @SerializedName("events")
    @Expose
    ArrayList<Event> events;

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    @Override
    public String toString() {
        return "EventsContainer{" +
                "events=" + events.toString() +
                '}';
    }
}
