package com.example.alejandro.mvvp.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.alejandro.mvvp.R;
import com.example.alejandro.mvvp.adapters.AdapterTeams;
import com.example.alejandro.mvvp.models.EventsContainer;
import com.example.alejandro.mvvp.models.League;
import com.example.alejandro.mvvp.models.Leagues;
import com.example.alejandro.mvvp.models.Team;
import com.example.alejandro.mvvp.models.Teams;
import com.example.alejandro.mvvp.presenters.LeagueListPresenter;
import com.example.alejandro.mvvp.views.BaseActivity;
import com.example.alejandro.mvvp.views.interfaces.ILeagueListInterface;

public class LeagueListActivity extends BaseActivity<LeagueListPresenter> implements ILeagueListInterface {

    private League leagues;
    private Spinner spinner;
    private Teams teams;
    private AdapterTeams adapterTeams;
    private RecyclerView recyclerView;
    private boolean firstLoad;
    private Team team;
    private EventsContainer events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.league_list);
        leagues = (League) getIntent().getSerializableExtra("leagues");
        teams = (Teams) getIntent().getSerializableExtra("teams");
        spinner = findViewById(R.id.league_name);
        recyclerView = findViewById(R.id.rvRecycler);
        this.firstLoad = true;
        setPresenter(new LeagueListPresenter());
        getPresenter().inject(this, getValidateInternet(), repository);
        fillSpinner();
        loadAdapterTeams();
    }

    private void loadAdapterTeams() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapterTeams = new AdapterTeams(teams, LeagueListActivity.this);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LeagueListActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(adapterTeams);
            }
        });
    }

    private void fillSpinner() {
        spinner.setAdapter(
                new ArrayAdapter<>(LeagueListActivity.this,
                        android.R.layout.simple_spinner_dropdown_item, leagues.getLeagues())
        );

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!firstLoad) {
                    Leagues league = (Leagues) spinner.getItemAtPosition(spinner.getSelectedItemPosition());
                    getPresenter().createThreadGetTeams(league.getIdLeague());
                } else {
                    firstLoad = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void fillLeague(Teams teams) {
        this.teams = teams;
        loadAdapterTeams();
    }

    @Override
    public void initDetailTeam(Team team) {
        this.team = team;
        getPresenter().createThreadGetEvents(team.getId());
    }

    @Override
    public void setEvents(EventsContainer events) {
        this.events = events;

    }

    @Override
    public void intentToDetailTeam() {
        Intent intent = new Intent(LeagueListActivity.this, DetailTeamActivity.class);
        intent.putExtra("team", team);
        intent.putExtra("events", events);
        startActivity(intent);
    }
}
