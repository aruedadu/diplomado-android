package com.example.alejandro.mvvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Teams implements Serializable {

    @SerializedName("teams")
    @Expose
    private ArrayList<Team> teams;

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }

    @Override
    public String toString() {
        return "Teams{" +
                "teams=" + teams.toString() +
                '}';
    }
}
