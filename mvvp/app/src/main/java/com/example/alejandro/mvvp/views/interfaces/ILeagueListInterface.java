package com.example.alejandro.mvvp.views.interfaces;

import com.example.alejandro.mvvp.models.EventsContainer;
import com.example.alejandro.mvvp.models.Team;
import com.example.alejandro.mvvp.models.Teams;
import com.example.alejandro.mvvp.views.IBaseView;

public interface ILeagueListInterface extends IBaseView {

    void fillLeague(Teams teams);

    void initDetailTeam(Team team);

    void setEvents(EventsContainer events);

    void intentToDetailTeam();
}
