package com.example.alejandro.mvvp.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.alejandro.mvvp.R;
import com.example.alejandro.mvvp.models.League;
import com.example.alejandro.mvvp.models.Teams;
import com.example.alejandro.mvvp.presenters.MainPresenter;
import com.example.alejandro.mvvp.views.BaseActivity;
import com.example.alejandro.mvvp.views.interfaces.IMainInterface;

import java.util.concurrent.ExecutionException;

public class MainActivity extends BaseActivity<MainPresenter> implements IMainInterface {

    private Teams teams;//informacion especifica de la liga
    private League league;//lista de ligas

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setPresenter(new MainPresenter());
        getPresenter().inject(this, getValidateInternet(), repository);
        startAnimations();
        getLeagueList();
    }

    private void startAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l = findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = findViewById(R.id.logo);
        iv.clearAnimation();
        iv.startAnimation(anim);
    }

    private void getLeagueList() {
        if (getValidateInternet().isConnected()) {
            createThreadGetLeagues();
        } else {
            showAlertDialog(getResources().getString(R.string.error),
                    getResources().getString(R.string.no_internet_conection));
        }
    }

    private void createThreadGetLeagues() {
        try {
            getPresenter().createThreadGetLeagues();
        } catch (ExecutionException e) {
            e.printStackTrace();
            showAlertDialog(getResources().getString(R.string.error), getResources().getString(R.string.default_error));
        } catch (InterruptedException e) {
            e.printStackTrace();
            showAlertDialog(getResources().getString(R.string.error), getResources().getString(R.string.default_error));
        }
    }

    @Override
    public void fillLeagues(League leagues){

        this.league = leagues;
    }

    @Override
    public void fillLeague(Teams teams) {
        this.teams = teams;
    }

    @Override
    public void goToListLeagues() {
        Intent intent = new Intent(MainActivity.this, LeagueListActivity.class);
        intent.putExtra("leagues", league);
        intent.putExtra("teams", teams);
        startActivity(intent);
        finish();
    }


}
