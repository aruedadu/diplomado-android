package com.example.alejandro.mvvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Team implements Serializable {
    /*
    idTeam	"134221"
    idSoccerXML	"676"
    intLoved	null
    strTeam	"Alaves"
    strTeamShort	null
    strAlternate	"Deportivo Alavés"
    intFormedYear	"1921"
    strSport	"Soccer"
    strLeague	"Spanish La Liga"
    idLeague	"4335"
    strDivision	null
    strManager	"Luis Zubeldía"
    strStadium	"Mendizorroza"
    strKeywords	""
    strRSS	""
    strStadiumThumb	"https://www.thesportsdb.…ium/vwtxsr1472387761.jpg"
    strStadiumDescription	"Estadio Mendizorrotza is…e current 19,840 seats."
    strStadiumLocation	"Vitoria"
    intStadiumCapacity	"19840"
    strWebsite	"www.alaves.com"
    strFacebook	""
    strTwitter	""
    strInstagram	""
    strDescriptionEN	"Deportivo Alavés, S.A.D.… dedicated to training."
    strDescriptionDE	null
    strDescriptionFR	null
    strDescriptionCN	null
    strDescriptionIT	null
    strDescriptionJP	null
    strDescriptionRU	null
    strDescriptionES	"El Deportivo Alavés, S.A…o por gol de oro (5-4)."
    strDescriptionPT	null
    strDescriptionSE	null
    strDescriptionNL	null
    strDescriptionHU	null
    strDescriptionNO	null
    strDescriptionIL	null
    strDescriptionPL	null
    strGender	"Male"
    strCountry	"Spain"
    strTeamBadge	"https://www.thesportsdb.…dge/vwqswq1420325494.png"
    strTeamJersey	"https://www.thesportsdb.…sey/tprvqx1472752261.png"
    strTeamLogo	"https://www.thesportsdb.…ogo/iq6a8b1527934454.png"
    strTeamFanart1	"https://www.thesportsdb.…art/wrpsts1472387855.jpg"
    strTeamFanart2	"https://www.thesportsdb.…art/xyuqvr1472388029.jpg"
    strTeamFanart3	"https://www.thesportsdb.…art/xqwuyy1472388289.jpg"
    strTeamFanart4	"https://www.thesportsdb.…art/fp357p1527863440.jpg"
    strTeamBanner	"https://www.thesportsdb.…ner/3axoi21504718301.jpg"
    strYoutube	""
    strLocked	"unlocked"
     */

    /*
    nombre equipo
    estadio
    escudo
     */

    @SerializedName("strTeam")
    @Expose
    private String teamName;
    @SerializedName("strStadium")
    @Expose
    private String stadium;
    @SerializedName("strTeamBadge")
    @Expose
    private String teamBadge;

    @SerializedName("strDescriptionEN")
    @Expose
    private String teamDescription;

    @SerializedName("strTeamJersey")
    @Expose
    private String teamJersey;

    @SerializedName("intFormedYear")
    @Expose
    private String formedYear;

    @SerializedName("strTeamBanner")
    @Expose
    private String teamBanner;


    @SerializedName("idTeam")
    @Expose
    private String id;

    @SerializedName("strInstagram")
    @Expose
    private String instagram;

    @SerializedName("strTwitter")
    @Expose
    private String twitter;

    @SerializedName("strFacebook")
    @Expose
    private String facebook;

    @SerializedName("strWebsite")
    @Expose
    private String webSite;


    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public String getTeamBadge() {
        return teamBadge;
    }

    public void setTeamBadge(String teamBadge) {
        this.teamBadge = teamBadge;
    }

    public String getTeamDescription() {
        return teamDescription;
    }

    public void setTeamDescription(String teamDescription) {
        this.teamDescription = teamDescription;
    }

    public String getTeamJersey() {
        return teamJersey;
    }

    public void setTeamJersey(String teamJersey) {
        this.teamJersey = teamJersey;
    }

    public String getFormedYear() {
        return formedYear;
    }

    public void setFormedYear(String formedYear) {
        this.formedYear = formedYear;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeamBanner() {
        return teamBanner;
    }

    public void setTeamBanner(String teamBanner) {
        this.teamBanner = teamBanner;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    @Override
    public String toString() {
        return "Team{" +
                "teamName='" + teamName + '\'' +
                ", stadium='" + stadium + '\'' +
                ", teamBadge='" + teamBadge + '\'' +
                ", teamDescription='" + teamDescription + '\'' +
                ", teamJersey='" + teamJersey + '\'' +
                ", formedYear='" + formedYear + '\'' +
                ", teamBanner='" + teamBanner + '\'' +
                ", id='" + id + '\'' +
                ", instagram='" + instagram + '\'' +
                ", twitter='" + twitter + '\'' +
                ", facebook='" + facebook + '\'' +
                ", webSite='" + webSite + '\'' +
                '}';
    }

}
