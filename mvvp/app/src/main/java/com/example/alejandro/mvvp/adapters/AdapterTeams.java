package com.example.alejandro.mvvp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alejandro.mvvp.R;
import com.example.alejandro.mvvp.models.Team;
import com.example.alejandro.mvvp.models.Teams;
import com.example.alejandro.mvvp.views.interfaces.ILeagueListInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterTeams extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Team> teams;

    private Context context;
    private ILeagueListInterface iLeagueListInterface;

    public AdapterTeams(Teams teams, ILeagueListInterface iLeagueListInterface) {
        this.teams = teams.getTeams();
        this.iLeagueListInterface = iLeagueListInterface;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_teams,
                parent, false);
        context = parent.getContext();
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CustomViewHolder customViewHolder = (CustomViewHolder) holder;
        final Team team = teams.get(position);
        customViewHolder.teamName.setText(team.getTeamName());
        customViewHolder.teamDescription.setText(team.getTeamName());
        Picasso.get().load(team.getTeamBadge()).into(customViewHolder.imageView);
        customViewHolder.teamDescription.setText(team.getStadium());
        customViewHolder.cvTeams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.err.println("************************ evento de click sobre el viewholder");
                iLeagueListInterface.initDetailTeam(team);
            }
        });
    }

    @Override
    public int getItemCount() {
        return null != teams?teams.size():0;
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView teamName;
        private TextView teamDescription;
        private ImageView imageView;
        private CardView cvTeams;

        public CustomViewHolder(View itemView) {
            super(itemView);
            teamName = itemView.findViewById(R.id.tv_team_name);
            teamDescription = itemView.findViewById(R.id.tv_team_desc);
            imageView = itemView.findViewById(R.id.iv_team);
            cvTeams = itemView.findViewById(R.id.cv_teams);
        }
    }

}
