package com.example.alejandro.mvvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class League implements Serializable {
    @SerializedName("leagues")
    @Expose
    public ArrayList<Leagues> leagues;

    public ArrayList<Leagues> getLeagues() {
        return leagues;
    }

    public void setLeagues(ArrayList<Leagues> leagues) {
        this.leagues = leagues;
    }

    @Override
    public String toString() {
        return "League{" +
                "leagues=" + leagues.toString() +
                '}';
    }
}
