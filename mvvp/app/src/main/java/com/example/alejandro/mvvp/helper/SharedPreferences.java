package com.example.alejandro.mvvp.helper;

import android.content.Context;

public class SharedPreferences {

    private android.content.SharedPreferences sharedPreferences;

    public SharedPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
    }

    public String getString(String key){
        if(sharedPreferences.contains(key)){
            return sharedPreferences.getString(key, null);
        }
        return null;
    }

    public void addString(String key, String value){
        if(null != value && null != key){
            addValue(key, value);
        }
    }

    private void addValue(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    private void deleteValue(String key){
        sharedPreferences.edit().remove(key).apply();
    }

}
