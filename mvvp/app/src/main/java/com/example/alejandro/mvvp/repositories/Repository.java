package com.example.alejandro.mvvp.repositories;

import com.example.alejandro.mvvp.helper.Constants;
import com.example.alejandro.mvvp.models.EventsContainer;
import com.example.alejandro.mvvp.models.League;
import com.example.alejandro.mvvp.models.Teams;
import com.example.alejandro.mvvp.services.IServices;
import com.example.alejandro.mvvp.services.ServicesFactory;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class Repository {

    private IServices iServices;

    public Repository() {
        ServicesFactory servicesFactory = new ServicesFactory();
        iServices = (IServices) servicesFactory.getInstanceService(IServices.class);
    }

    private IOException defaultError() {
        return new IOException(Constants.DEFAULT_ERROR);
    }

    public League getLeagues() throws IOException {
        try {
            Call<League> call = iServices.getLeagues();
            Response<League> response = call.execute();
            if (null != response.errorBody()) {
                throw this.defaultError();
            } else {
                return response.body();
            }

        } catch (IOException e) {
            throw this.defaultError();
        }
    }

    public Teams getTeams(String id) throws IOException {
        try {
            Call<Teams> call = iServices.getTeams(id);
            Response<Teams> response = call.execute();
            //System.err.println("************************ "+call.request().url());
            if (null != response.errorBody()) {
                throw this.defaultError();
            } else {
                return response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw this.defaultError();
        }
    }

    public EventsContainer getNextFiveEvents(String id) throws IOException {
        try{
            Call<EventsContainer> call = iServices.getNextFiveEvents(id);
            Response<EventsContainer> response = call.execute();
            if (null != response.errorBody()) {
                throw this.defaultError();
            } else {
                return response.body();
            }
        }catch(IOException e){
            throw this.defaultError();
        }
    }

}
