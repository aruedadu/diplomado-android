package com.example.alejandro.mvvp.views;

public interface IBaseView {
    public void showAlertDialog(final String tittle, final String message);

    public void showAlertDialogUiThread(String tittle, String message);
}
